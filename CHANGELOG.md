# Changelog

## 1.1.0
* Supports reading the status register
* Supports clearing the status register

## 1.0.0
* Supports SHT3x Temperature/Humidity Sensor
* Supports STS3x Temperature Sensor
* Provides single sensor reading with low, medium and high Repeatability
* Provides reading the sensors serial number
