# sensirion-rht

This crate implements multiple Temperature and Humidity Sensors from [Sensirion](https://sensirion.com/). Multiple Sensors are combined into one single crate since the sensors share a common API.

## Supported Sensors:
| Sensor   |      Kind      |  Link |
|:----------:|:-------------:|:------:|
| SHT3x |  Humidity and Temperature | [Sensirion SHT3x](https://sensirion.com/de/produkte/katalog/?filter_series=370b616d-de4c-469f-a22b-e5e8737481b5) |
| STS3x |    Temperature   |   [Sensirion STS3x](https://sensirion.com/de/produkte/katalog/?filter_series=a434ee4e-264e-4c38-a783-5ece5d479c11) |

## Usage Example

The SHT3X returns both temperature and humidity

```rust
use sensirion_rht::*;
let mut sensor = Device::new_sht3x(Addr::A, i2c, delay);

if let Ok((temperature, humidity)) = sensor.single_shot(Repeatability::High) {
  log::info!(
    "Temperature: {}, Humidity: {}",
    temperature,
    humidity
  );
}
```

The STS3X returns only temperature

```rust
use sensirion_rht::*;
let mut sensor = Device::new_sts3x(Addr::A, i2c, delay);

if let Ok(temperature) = sensor.single_shot(Repeatability::High) {
  log::info!(
    "Temperature: {}",
    temperature
  );
}
```

## License

Open Logistics Foundation License\
Version 1.3, January 2023

See the LICENSE file in the top-level directory.

## Contact

Fraunhofer IML Embedded Rust Group - <embedded-rust@iml.fraunhofer.de>
