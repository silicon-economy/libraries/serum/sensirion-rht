// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

#![warn(missing_docs)]
#![cfg_attr(not(test), no_std)]

//! This crate implements multiple Temperature and Humidity Sensors from [Sensirion](https://sensirion.com/). Multiple Sensors are combined into one single crate since the sensors share a common API.
//!
//! # Supported Sensors:
//! | Sensor   |      Kind      |  Link |
//! |:----------:|:-------------:|:------:|
//! | SHT3x |  Humidity and Temperature | [Sensirion SHT3x](https://sensirion.com/de/produkte/katalog/?filter_series=370b616d-de4c-469f-a22b-e5e8737481b5) |
//! | STS3x |    Temperature   |   [Sensirion STS3x](https://sensirion.com/de/produkte/katalog/?filter_series=a434ee4e-264e-4c38-a783-5ece5d479c11) |
//!
//! # Usage Example
//!
//! The SHT3X returns both temperature and humidity
//!
//! ```rust,no_run
//! # use embedded_hal_mock::*;
//! # let expectations = [];
//! # let mut i2c = i2c::Mock::new(&expectations);
//! # let mut delay = delay::MockNoop::new();
//! use sensirion_rht::*;
//! let mut sensor = Device::new_sht3x(Addr::A, i2c, delay);
//!
//! if let Ok((temperature, humidity)) = sensor.single_shot(Repeatability::High) {
//!   log::info!(
//!     "Temperature: {}, Humidity: {}",
//!     temperature,
//!     humidity
//!   );
//! }
//! ```
//!
//! The STS3X returns only temperature
//!
//! ```rust,no_run
//! # use embedded_hal_mock::*;
//! # let expectations = [];
//! # let mut i2c = i2c::Mock::new(&expectations);
//! # let mut delay = delay::MockNoop::new();
//! use sensirion_rht::*;
//! let mut sensor = Device::new_sts3x(Addr::A, i2c, delay);
//!
//! if let Ok(temperature) = sensor.single_shot(Repeatability::High) {
//!   log::info!(
//!     "Temperature: {}",
//!     temperature
//!   );
//! }
//! ```
//!
//! # License
//!
//! Open Logistics Foundation License\
//! Version 1.3, January 2023
//!
//! See the LICENSE file in the top-level directory.
//!
//! # Contact
//!
//! Fraunhofer IML Embedded Rust Group - <embedded-rust@iml.fraunhofer.de>

use embedded_hal::blocking::delay::DelayMs;
use embedded_hal::blocking::i2c;

use byteorder::{BigEndian, ByteOrder};

use measurements::{Humidity, Temperature};
/// Minimal Waiting time between two Command
///
/// The Module will block after every command for at least this time.
/// According to the documentation by Sensirion, it should be 1ms
/// but somehow this seems to be too little sometimes. But it is 1ms anyways for now.
/// This value was found through "try and error"
static MIN_WAIT_BETWEEN_COMMAND_MS: u32 = 1;

/// I2C Address of the sensor in use, determined by the sensors address pin
#[derive(Debug, Copy, Clone)]
pub enum Addr {
    /// SHT3X: 0x44, STS3X: 0x4A
    A,
    /// SHT3X: 0x45, STS3X: 0x4B
    B,
}

/// Measurement duration and thus energy use
///
/// The time in ms is the time a measurement can take.
#[derive(Debug, Copy, Clone)]
pub enum Repeatability {
    /// 3 times the standard deviation: 0.15 °C, 0.21 %RH
    Low = 5,
    /// 3 times the standard deviation: 0.08 °C, 0.15 %RH
    Medium = 7,
    /// 3 times the standard deviation: 0.04 °C, 0.08 %RH
    High = 16,
}

/// Possible errors in this driver
#[derive(Debug, Copy, Clone)]
pub enum Error<E> {
    /// An I2C Error occurred
    I2C(E),
    /// A read timed out
    Timeout,
    /// A checksum could not be verified
    CRC,
}

impl<W, R, E> From<sensirion_i2c::i2c::Error<W, R>> for Error<E>
where
    R: i2c::Read<Error = E>,
    W: i2c::Write<Error = E>,
{
    fn from(e: sensirion_i2c::i2c::Error<W, R>) -> Self {
        use sensirion_i2c::i2c;
        match e {
            i2c::Error::Crc => Self::CRC,
            i2c::Error::I2cRead(e) => Self::I2C(e),
            i2c::Error::I2cWrite(e) => Self::I2C(e),
        }
    }
}

/// Internal Command Type
///
/// It represents all Commands according to https://sensirion.com/media/documents/213E6A3B/61641DC3/Sensirion_Humidity_Sensors_SHT3x_Datasheet_digital.pdf
/// and https://sensirion.com/media/documents/1DA31AFD/61641F76/Sensirion_Temperature_Sensors_STS3x_Datasheet.pdf
#[derive(Debug, Copy, Clone)]
#[allow(dead_code)]
enum Command {
    /// Triggers a single measurement with given Repeatability and ClockStretching enable or disabled.
    SingleShot(ClockStretching, Repeatability),
    /// Not yet implemented.
    /// Starts periodic Data Acquisition at given Frequency with given Repeatability
    Periodic(DataAcquisitionFrequency, Repeatability),
    /// Not yet implemented.
    /// Fetches latest Data read by the Sensor, NAK if no Data is present
    FetchData,
    /// Not yet implemented.
    /// Alert Mode according to https://sensirion.com/media/documents/40D749F7/616400BB/Sensirion_Humidity_Sensors_SHT3x_Application_Note_Alert_Mode_DIS.pdf
    // TODO Is the alert mode also available for the STS?
    PeriodicWithART,
    /// Not yet implemented.
    /// Stops periodic Data Acquisition
    Break,
    /// Not yet implemented.
    /// Performs a Software Reset
    SoftReset,
    /// Not yet implemented.
    /// Enables the internal Heater
    HeaterEnable,
    /// Not yet implemented.
    /// Disables the internal Heater
    HeaterDisable,
    /// Reads the internal Status Register
    Status,
    /// Clears the internal Status Register
    ClearStatus,
    /// Request the sensors Serial Number
    /// Not stated in the datasheets, taken from:
    /// https://www.sensirion.com/fileadmin/user_upload/customers/sensirion/Dokumente/2_Humidity_Sensors/Software/Sensirion_Humidity_Sensors_Software_SHT3x_Sample_Code.zip
    SerialNumber,
}

impl Command {
    /// Converts the Command into a actual command u16 that is send to the sensor
    fn as_u16(&self) -> u16 {
        byteorder::BigEndian::read_u16(&self.as_bytes())
    }

    /// Converts the Command into a actual command bytes that is send to the sensor
    fn as_bytes(&self) -> [u8; 2] {
        match *self {
            Self::SingleShot(ClockStretching::Enabled, rep) => [
                0x2C,
                match rep {
                    Repeatability::Low => 0x10,
                    Repeatability::Medium => 0x0D,
                    Repeatability::High => 0x06,
                },
            ],
            Self::SingleShot(ClockStretching::Disabled, rep) => [
                0x24,
                match rep {
                    Repeatability::Low => 0x16,
                    Repeatability::Medium => 0x0B,
                    Repeatability::High => 0x00,
                },
            ],
            Self::Periodic(DataAcquisitionFrequency::Half, rep) => [
                0x20,
                match rep {
                    Repeatability::Low => 0x2F,
                    Repeatability::Medium => 0x24,
                    Repeatability::High => 0x32,
                },
            ],
            Self::Periodic(DataAcquisitionFrequency::One, rep) => [
                0x21,
                match rep {
                    Repeatability::Low => 0x2D,
                    Repeatability::Medium => 0x26,
                    Repeatability::High => 0x30,
                },
            ],
            Self::Periodic(DataAcquisitionFrequency::Two, rep) => [
                0x22,
                match rep {
                    Repeatability::Low => 0x2B,
                    Repeatability::Medium => 0x20,
                    Repeatability::High => 0x36,
                },
            ],
            Self::Periodic(DataAcquisitionFrequency::Four, rep) => [
                0x23,
                match rep {
                    Repeatability::Low => 0x29,
                    Repeatability::Medium => 0x22,
                    Repeatability::High => 0x34,
                },
            ],
            Self::Periodic(DataAcquisitionFrequency::Ten, rep) => [
                0x27,
                match rep {
                    Repeatability::Low => 0x2A,
                    Repeatability::Medium => 0x21,
                    Repeatability::High => 0x37,
                },
            ],
            Self::FetchData => [0xE0, 0x00],
            Self::PeriodicWithART => [0x2B, 0x32],
            Self::Break => [0x30, 0x93],
            Self::SoftReset => [0x30, 0xA2],
            Self::HeaterEnable => [0x30, 0x6D],
            Self::HeaterDisable => [0x30, 0x66],
            Self::Status => [0xF3, 0x2D],
            Self::ClearStatus => [0x30, 0x41],
            Self::SerialNumber => [0x37, 0x80],
        }
    }
}

/// ClockStretching Support
#[derive(Debug, Copy, Clone)]
#[allow(dead_code)]
enum ClockStretching {
    /// ClockStretching Enabled
    Enabled,
    /// ClockStretching Disabled
    Disabled,
}

/// Periodic Data Acquisition Frequency [Hz]
#[derive(Debug, Copy, Clone)]
#[allow(dead_code)]
enum DataAcquisitionFrequency {
    /// 0.5 measurements per second
    Half,
    /// 1 measurement per second
    One,
    /// 2 measurements per second
    Two,
    /// 4 measurements per second
    Four,
    /// 10 measurements per second
    Ten,
}

/// Module containing markers to decide between SHT3X and STS3X implementation
pub mod kind {
    use super::*;
    /// Trait marking the different sensor kinds. Requires you to define
    pub trait SensirionSensor {
        /// Returns the I2C address(es) for the given sensor
        fn addr_value(addr: Addr) -> u8;
    }
    /// Humidity and temperature sensor
    pub struct SHT3x {}
    impl SensirionSensor for SHT3x {
        fn addr_value(addr: Addr) -> u8 {
            match addr {
                Addr::A => 0x44,
                Addr::B => 0x45,
            }
        }
    }
    /// Temperature sensor
    pub struct STS3x;
    impl SensirionSensor for STS3x {
        fn addr_value(addr: Addr) -> u8 {
            match addr {
                Addr::A => 0x4A,
                Addr::B => 0x4B,
            }
        }
    }
}

/// Internal Status Register of the Sensor
#[derive(Debug)]
pub struct StatusRegister {
    /// At least one alert is pending
    pub has_alert_pending: bool,
    /// Heater is active
    pub is_heater_on: bool,
    /// Sensor has an humidity tracking alert pending
    pub has_rh_tracking_alert: bool,
    /// Sensor has a temperature tracking alert pending
    pub has_t_tracking_alert: bool,
    /// System Reset (hard reset, soft reset, command or supply fail) detected since last status clear
    pub system_reset_detected: bool,
    /// Last command was either invalid or failed the integrated command checksum
    pub last_command_failed: bool,
    /// Checksum of last write transfer failed
    pub last_checksum_failed: bool,
}

/// Common implementation for SHT/STS. Stores the address and device peripherals.
#[derive(Debug)]
pub struct Device<I2C, Delay, Kind>
where
    Kind: kind::SensirionSensor,
{
    addr: Addr,
    i2c: I2C,
    delay: Delay,
    _kind: core::marker::PhantomData<Kind>,
}

impl<I2C, Delay, Kind, E> Device<I2C, Delay, Kind>
where
    I2C: i2c::Read<Error = E> + i2c::Write<Error = E>,
    Delay: DelayMs<u32>,
    Kind: kind::SensirionSensor,
{
    /// Create a new instance of this sensor.
    fn new(addr: Addr, i2c: I2C, delay: Delay) -> Self {
        Self {
            addr,
            i2c,
            delay,
            _kind: core::marker::PhantomData,
        }
    }

    /// Destroy the sensor and return the hardware peripherals
    pub fn destroy(self) -> (I2C, Delay) {
        (self.i2c, self.delay)
    }

    /// Helper function to ensure that we do not forget to wait after the command
    fn read_words_with_crc_and_wait(&mut self, addr: u8, data: &mut [u8]) -> Result<(), Error<E>> {
        let result = sensirion_i2c::i2c::read_words_with_crc(&mut self.i2c, addr, data);
        self.delay.delay_ms(MIN_WAIT_BETWEEN_COMMAND_MS);
        Ok(result?)
    }

    /// Send a command to the device and retrieve a response from it
    fn command(
        &mut self,
        addr: u8,
        command: u16,
        data: &mut [u8],
        timeout: u32,
    ) -> Result<(), Error<E>> {
        sensirion_i2c::i2c::write_command(&mut self.i2c, addr, command).map_err(Error::I2C)?;

        let mut waited = 0;
        loop {
            // TODO: To be completely correct, we would also have to incorporate the time this
            // function call takes into our waited variable.
            if self.read_words_with_crc_and_wait(addr, data).is_ok() {
                return Ok(());
            }
            waited += MIN_WAIT_BETWEEN_COMMAND_MS;
            if waited > timeout {
                return Err(Error::Timeout);
            }
        }
    }

    /// Do a single shot measurement. The size of the data array determines the amount of data to
    /// be read. Every 3rd element in the data array is a crc value, so if you want to read two
    /// bytes, the size has to be 3, 4 bytes: size of 6 etc
    fn command_single_shot<const DATA_SIZE: usize>(
        &mut self,
        repeatability: Repeatability,
    ) -> Result<[u8; DATA_SIZE], Error<E>> {
        let addr = self.addr_value();
        let maximum_repeat_time = repeatability as u32;
        let mut data = [0_u8; DATA_SIZE];

        self.command(
            addr,
            Command::SingleShot(ClockStretching::Disabled, repeatability).as_u16(),
            &mut data,
            maximum_repeat_time,
        )
        .map(|_| data)
    }

    /// Getting the status register.
    pub fn status(&mut self) -> Result<StatusRegister, Error<E>> {
        let addr = self.addr_value();
        let mut data = [0_u8; 3];

        self.command(addr, Command::Status.as_u16(), &mut data, 0)?;

        let register = BigEndian::read_u16(&data[..=1]);

        Ok(StatusRegister {
            has_alert_pending: ((1 << 15) & register) != 0,
            is_heater_on: ((1 << 13) & register) != 0,
            has_rh_tracking_alert: ((1 << 11) & register) != 0,
            has_t_tracking_alert: ((1 << 10) & register) != 0,
            system_reset_detected: ((1 << 4) & register) != 0,
            last_command_failed: ((1 << 1) & register) != 0,
            last_checksum_failed: (1 & register) != 0,
        })
    }

    /// Clearing the status register
    pub fn clear_status(&mut self) -> Result<(), Error<E>> {
        let addr = self.addr_value();
        let command = Command::ClearStatus.as_u16();

        sensirion_i2c::i2c::write_command(&mut self.i2c, addr, command).map_err(Error::I2C)?;
        Ok(())
    }

    fn addr_value(&self) -> u8 {
        Kind::addr_value(self.addr)
    }

    /// Get the devices serial number
    pub fn read_serial_number(&mut self) -> Result<u32, Error<E>> {
        self.i2c
            .write(self.addr_value(), &Command::SerialNumber.as_bytes())
            .map_err(Error::I2C)?;

        self.delay.delay_ms(MIN_WAIT_BETWEEN_COMMAND_MS);

        let mut serial_number: [u8; 4] = [0; 4];

        self.i2c
            .read(self.addr_value(), &mut serial_number)
            .map_err(Error::I2C)?;

        self.delay.delay_ms(MIN_WAIT_BETWEEN_COMMAND_MS);

        Ok(u32::from_be_bytes(serial_number))
    }
}

/// Only available in SHT3X
impl<I2C, Delay, E> Device<I2C, Delay, kind::SHT3x>
where
    I2C: i2c::Read<Error = E> + i2c::Write<Error = E>,
    Delay: DelayMs<u32>,
{
    /// Do a single shot measurement with a given repeatability
    pub fn single_shot(
        &mut self,
        repeatability: Repeatability,
    ) -> Result<(Temperature, Humidity), Error<E>> {
        let read_result = self.command_single_shot::<6>(repeatability);
        read_result.map(|data| {
            (
                temperature_from_raw(byteorder::BigEndian::read_u16(&data[0..2])), // first word is temperature
                humidity_from_raw(byteorder::BigEndian::read_u16(&data[3..5])), // second word (with crc in between) is humidity
            )
        })
    }
    /// Create a new instance of a SHT3x sensor.
    pub fn new_sht3x(addr: Addr, i2c: I2C, delay: Delay) -> Self {
        Self::new(addr, i2c, delay)
    }
}

/// Only available in STS3X
impl<I2C, Delay, E> Device<I2C, Delay, kind::STS3x>
where
    I2C: i2c::Read<Error = E> + i2c::Write<Error = E>,
    Delay: DelayMs<u32>,
{
    /// Do a single shot measurement with a given repeatability
    pub fn single_shot(&mut self, repeatability: Repeatability) -> Result<Temperature, Error<E>> {
        let read_result = self.command_single_shot::<3>(repeatability);
        read_result.map(|data| temperature_from_raw(byteorder::BigEndian::read_u16(&data[0..2])))
    }

    /// Create a new instance of a STS3x sensor.
    pub fn new_sts3x(addr: Addr, i2c: I2C, delay: Delay) -> Self {
        Self::new(addr, i2c, delay)
    }
}

/// calculate temperature [degC]
fn temperature_from_raw(raw: u16) -> Temperature {
    // T = -45 + 175 * rawValue / (2^16-1)
    Temperature::from_celsius(175.0 * raw as f64 / 65535.0 - 45.0)
}

/// calculate relative humidity [%RH]
fn humidity_from_raw(raw: u16) -> Humidity {
    // RH = rawValue / (2^16-1) * 100
    Humidity::from_percent(100.0 * raw as f64 / 65535.0)
}
